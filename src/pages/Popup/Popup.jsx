import { Alert, Snackbar, Typography } from '@mui/material';
import React, { useEffect, useState } from 'react';

import Autocomplete from '@mui/material/Autocomplete';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Checkbox from '@mui/material/Checkbox';
import Chip from '@mui/material/Chip';
import FormControlLabel from '@mui/material/FormControlLabel';
import FormGroup from '@mui/material/FormGroup';
import TextField from '@mui/material/TextField';

const events = ['click', 'change', 'mouseover', 'input'];

const Popup = () => {
  const [selectedEvents, setSelectedEvents] = useState([]);
  const [isDebounceEnabled, setDebounce] = useState(true);
  const [isToastOpen, setToastOpen] = useState(false);

  const handleChange = (_, value, reason) => {
    if (reason !== 'createOption') {
      setSelectedEvents(value);
    }
  };

  const handleCheckboxChange = (_, value) => {
    setDebounce(value);
  };

  const handleSave = () => {
    chrome.storage.local.set({ selectedEvents, isDebounceEnabled }, () => {
      setToastOpen(true);
      setTimeout(() => {
        setToastOpen(false);
      }, 2000);
      console.log('data stored');
    });
  };

  useEffect(() => {
    chrome.storage.local.get(
      ['selectedEvents', 'isDebounceEnabled'],
      (data) => {
        let { selectedEvents = [], isDebounceEnabled = true } = data;
        setSelectedEvents(selectedEvents);
        setDebounce(isDebounceEnabled);
      }
    );
  }, []);

  return (
    <>
      <Typography sx={{ p: 2 }}>Event configuration</Typography>
      <Box sx={{ p: 2, mt: 2 }}>
        <Autocomplete
          multiple
          id="tags-filled"
          options={events}
          value={selectedEvents}
          freeSolo
          onChange={handleChange}
          renderTags={(value, getTagProps) =>
            value.map((option, index) => (
              <Chip
                variant="outlined"
                label={option}
                {...getTagProps({ index })}
              />
            ))
          }
          renderInput={(params) => (
            <TextField
              {...params}
              variant="filled"
              label="Events to be captured"
              placeholder="Events"
            />
          )}
        />
        <FormGroup sx={{ mt: 2 }}>
          <FormControlLabel
            control={
              <Checkbox
                checked={isDebounceEnabled}
                onChange={handleCheckboxChange}
              />
            }
            label="Use debounce for risky events"
          />
        </FormGroup>
        <Box>
          <Button variant="contained" onClick={handleSave} sx={{ mt: 2 }}>
            Save changes
          </Button>
        </Box>
        <Box sx={{ mt: 2 }}>
          <Button
            variant="contained"
            onClick={() => {
              chrome.tabs.create({
                url: 'chrome://extensions/?options=' + chrome.runtime.id,
              });
            }}
          >
            Results
          </Button>
        </Box>
      </Box>
      <Snackbar
        anchorOrigin={{ vertical: 'top', horizontal: 'center' }}
        open={isToastOpen}
        key={`top center`}
        autoHideDuration={2000}
      >
        <Alert severity={'success'}>{'Changes saved successfully'}</Alert>
      </Snackbar>
    </>
  );
};

export default Popup;
