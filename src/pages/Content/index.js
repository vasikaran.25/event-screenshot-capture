const debounce = (func, delay) => {
  let timeout;
  return (...args) => {
    clearTimeout(timeout);
    timeout = setTimeout(() => {
      func(...args);
    }, delay);
  };
};

let getPathTo = (element) => {
  if (element.tagName == 'HTML') return '/HTML[1]';
  if (element === document.body) return '/HTML[1]/BODY[1]';

  var ix = 0;
  var siblings = element.parentNode.childNodes;
  for (var i = 0; i < siblings.length; i++) {
    var sibling = siblings[i];
    if (sibling === element)
      return (
        getPathTo(element.parentNode) +
        '/' +
        element.tagName +
        '[' +
        (ix + 1) +
        ']'
      );
    if (sibling.nodeType === 1 && sibling.tagName === element.tagName) ix++;
  }
};

let styleStrToObj = (styleStr) => {
  let styleObj = {};
  let attributes = styleStr.split(';');
  for (var i = 0; i < attributes.length; i++) {
    let entry = attributes[i].split(':');
    styleObj[entry.splice(0, 1)[0]] = entry.join(':').trim();
  }
  return styleObj;
};

let initMutationObserver = () => {
  const targetNode = document.body;
  const config = {
    attributes: true,
    childList: true,
    subtree: true,
    attributeOldValue: true,
  };

  let results = [];

  const callback = function (mutationsList) {
    for (const mutation of mutationsList) {
      let { oldValue, target, type, attributeName, addedNodes } = mutation;
      if (type === 'attributes' && attributeName === 'style') {
        let style = styleStrToObj(oldValue);
        if (
          oldValue &&
          style.display === 'none' &&
          target.style.display !== 'none'
        ) {
          results.push(target);
        } else if (
          oldValue &&
          style.visibility === 'hidden' &&
          target.style.visibility !== 'hidden'
        ) {
          results.push(target);
        }
      } else if (type === 'childList' && addedNodes.length) {
        results.push(...addedNodes);
      }
    }
  };

  const observer = new MutationObserver(callback);
  observer.observe(targetNode, config);
  return () => {
    observer.disconnect();
    // console.log(results, 'results');
    return results;
  };
};

let eventHandler = (event) => {
  let { top, left, height, width } = event.target.getBoundingClientRect();
  if (height > window.innerHeight) {
    height = window.innerHeight - height;
  }
  let xPath = getPathTo(event.target);
  let disconnect = initMutationObserver();
  chrome.runtime.sendMessage(
    { chromeAction: 'screenshot' },
    function (imageString) {
      let promises = [];
      let p1 = new Promise((resolve) => {
        createImage(imageString, { top, left, height, width }).then((url) => {
          resolve({
            src: url,
            xPath,
            eventName: event.type,
          });
          console.log(url, 'url');
        });
      });
      promises.push(p1);
      let p2 = new Promise((p2Resolve) => {
        setTimeout(() => {
          let results = disconnect();
          console.log(results, 'results images');
          let promises = [];
          let imageObject = [];
          let ssPromise = new Promise((ssResolve) => {
            chrome.runtime.sendMessage(
              { chromeAction: 'screenshot' },
              function (imageString) {
                results.forEach((element) => {
                  let p = new Promise((resolve) => {
                    let { top, left, height, width } =
                      element.getBoundingClientRect();
                    if (height > window.innerHeight) {
                      height = window.innerHeight - height;
                    }
                    let xPath = getPathTo(element);
                    createImage(imageString, { top, left, height, width }).then(
                      (url) => {
                        console.log(url, 'url');
                        imageObject.push({
                          src: url,
                          xPath,
                          eventName: 'Subsequent action',
                        });
                        resolve();
                      }
                    );
                  });
                  promises.push(p);
                });
                ssResolve();
              }
            );
          });
          ssPromise.then(() => {
            Promise.all(promises).then((result) => {
              console.log(imageObject, 'result inside');
              p2Resolve(imageObject);
            });
          });
        }, 1000);
      });
      promises.push(p2);
      Promise.all(promises).then((result) => {
        let [mainImage, subImages = []] = result;
        console.log(result, 'result');
        setTimeout(() => {
          chrome.runtime.sendMessage({
            chromeAction: 'save_image',
            data: {
              image: mainImage,
              subImages,
            },
          });
        }, 100);
      });
      return true;
    }
  );
};

function createImage(dataURL, { top, left, height, width }) {
  return new Promise((resolve) => {
    var canvas = createCanvas(width, height);
    var context = canvas.getContext('2d');
    var croppedImage = new Image();

    croppedImage.onload = function () {
      context.drawImage(
        croppedImage,
        left,
        top,
        width,
        height,
        0,
        0,
        width,
        height
      );
      resolve(canvas.toDataURL());
    };
    croppedImage.src = dataURL;
  });
}

function createCanvas(canvasWidth, canvasHeight) {
  var canvas = document.createElement('canvas');
  canvas.width = canvasWidth;
  canvas.height = canvasHeight;
  return canvas;
}

let debouncedHandler = debounce(eventHandler, 200);
let debouncedEvents = ['input', 'change', 'mouseover'];

chrome.storage.local.get(['selectedEvents', 'isDebounceEnabled'], (data) => {
  let { selectedEvents = [], isDebounceEnabled = true } = data;
  selectedEvents.forEach((event) => {
    if (debouncedEvents.includes(event) && isDebounceEnabled) {
      document.addEventListener(event, debouncedHandler, true);
    } else {
      document.addEventListener(event, eventHandler, true);
    }
  });
  initMutationObserver();
});
