import './index.css';

import Options from './Options';
import React from 'react';
import { render } from 'react-dom';

render(<Options />, window.document.querySelector('#app-container'));

if (module.hot) module.hot.accept();
