import React, { useEffect, useState } from 'react';

import Box from '@mui/material/Box';
import Divider from '@mui/material/Divider';
import FormControl from '@mui/material/FormControl';
import Grid from '@mui/material/Grid';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import Select from '@mui/material/Select';

const Options = () => {
  const [url, setUrl] = useState('');
  const [images, setImages] = useState({});

  const handleChange = (event) => {
    setUrl(event.target.value);
  };

  useEffect(() => {
    chrome.storage.local.get(['images'], function ({ images: items = {} }) {
      let urls = Object.keys(items);
      console.log(items);
      let imageUrls = {};
      urls.forEach((url) => {
        let images = items[url] || [];
        console.log(images, 'images');
        imageUrls[url] = [];
        images.forEach((image, index) => {
          imageUrls[url].push(image);
        });
      });
      setImages(imageUrls);
      let defaultUrl = Object.keys(imageUrls)[0];
      setUrl(defaultUrl);
    });
  }, []);

  return (
    <>
      <Box sx={{ minWidth: 120 }}>
        <FormControl>
          <InputLabel id="demo-simple-select-label">URL</InputLabel>
          <Select
            labelId="demo-simple-select-label"
            id="demo-simple-select"
            value={url}
            label="URL"
            onChange={handleChange}
          >
            {Object.keys(images).map((url, index) => {
              return (
                <MenuItem value={url} key={index}>
                  {url}
                </MenuItem>
              );
            })}
          </Select>
        </FormControl>
      </Box>
      {images[url] &&
        images[url].map((image, index) => {
          let { src, xPath, eventName, subImages } = image;
          return (
            <Box key={index} sx={{ border: '1px solid' }}>
              <Grid container sx={{ p: 5 }}>
                <Grid item xs={12}>
                  <Box
                    sx={{
                      display: 'flex',
                      flexDirection: 'column',
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}
                  >
                    <img src={src} key={index}></img>
                    <Box sx={{ mt: 10 }}>
                      <span>X Path : </span>
                      <span>{xPath}</span>
                    </Box>
                    <Box>
                      <span>Event Name : </span>
                      <span>{eventName}</span>
                    </Box>
                  </Box>
                </Grid>
              </Grid>
              <Divider />
              {subImages &&
                subImages.map((image, index) => {
                  let { src, xPath, eventName } = image;
                  return (
                    <React.Fragment key={index}>
                      <Grid container sx={{ p: 5 }}>
                        <Grid item xs={12}>
                          <Box
                            sx={{
                              display: 'flex',
                              flexDirection: 'column',
                              justifyContent: 'center',
                              alignItems: 'center',
                            }}
                          >
                            <img src={src} key={index}></img>
                            <Box sx={{ mt: 10 }}>
                              <span>X Path : </span>
                              <span>{xPath}</span>
                            </Box>
                            <Box>
                              <span>Event Name : </span>
                              <span>{eventName}</span>
                            </Box>
                          </Box>
                        </Grid>
                      </Grid>
                      <Divider />
                    </React.Fragment>
                  );
                })}
            </Box>
          );
        })}
    </>
  );
};

export default Options;
