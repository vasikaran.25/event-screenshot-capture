const saveImages = (url, image, subImages) => {
  let { src, xPath, eventName } = image;
  chrome.storage.local.get(['images'], function ({ images: data = {} }) {
    let images = data[url] || [];
    chrome.storage.local.set(
      {
        ['images']: Object.assign({}, data, {
          [url]: [...images, { src, xPath, eventName, subImages }],
        }),
      },
      function () {
        chrome.notifications.create(
          'save-screenshot',
          {
            type: 'basic',
            iconUrl: 'icon-34.png',
            title: 'Screenshot captured',
            message: `X-Path : ${xPath}`,
          },
          function (...args) {
            console.log(...args);
          }
        );
        console.log('data stored');
      }
    );
  });
};

chrome.runtime.onMessage.addListener(function (request, sender, sendResponse) {
  let { chromeAction } = request;
  if (chromeAction === 'screenshot') {
    createScreenshot(function (dataURL) {
      sendResponse(dataURL);
      return true;
    });
    return true;
  } else if (chromeAction === 'save_image') {
    let {
      tab: { url },
    } = sender;
    let {
      data: { image, subImages },
    } = request;
    saveImages(url, image, subImages);
    return true;
  }
  return true;
});

function createScreenshot(callback) {
  chrome.tabs.captureVisibleTab(null, { format: 'png' }, callback);
}
