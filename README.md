# Event & Screenshot Capture Chrome Extension

This Chrome Extension capture user's event and take screenshot of the HTML element and store it in the storage. Users can see the results in the reports page aka options page of this extension.

# Features
* Event tracking
* Screenshot capture for the event's HTML
* Capture screenshot for resultant UI of an event such as Popup, Modal, Tooltip (Experimental)

# Installation

Clone Repo
```
git clone https://gitlab.com/vasikaran.25/event-screenshot-capture.git
```

Go to `event-screenshot-capture` directory and run
```
npm install
```

Now build the extension using
```
npm run start
```

# Adding this extension to Chrome

Please refer [this link](https://developer.chrome.com/docs/extensions/mv3/getstarted/#unpacked) for adding this extension to Chrome browser

Note: You have to load `build` folder from the extension directory which is created by `npm run start` command

# Configure the Extension
* Once you install the extension click the extension icon in the menu bar. You can see a popup will be appear

[![Screenshot-2022-05-11-at-2-43-40-PM.png](https://i.postimg.cc/F1tvPWHw/Screenshot-2022-05-11-at-2-43-40-PM.png)](https://postimg.cc/2Lxgyxvx)

* You can configure which events should be captured here
* Note: There is an option named `Use Debounce for risky events`. Don't uncheck this option unless you don't need. If you uncheck this, it leads to performance brakage for your active tab. 

# Tracking events & Screenshot HTML
* You must open the extension popup on a particular tab which is you want to track & take screenshot.
* Note: It must be a irrelevent need. Will try to sort this out

# Reports page
* Go to results page you'll see the list of events you're tracking on with screenshots.
[![Screenshot-2022-05-11-at-2-45-41-PM.png](https://i.postimg.cc/rwfVT9nR/Screenshot-2022-05-11-at-2-45-41-PM.png)](https://postimg.cc/LJZKBjMm)
* Note: This results list based on websites you using this. You can change the website URL in the dropdown and see the results of particular website.

# Resources
* I'm using [React chrome extension boilerplate](https://github.com/lxieyang/chrome-extension-boilerplate-react) for develop this extension
* For capture screenshot such as popup I'm using [MutationObserver API](https://developer.mozilla.org/en-US/docs/Web/API/MutationObserver)
